import unittest
from mars_rover.position import Position

class TestPosition(unittest.TestCase):
    def test_position_with_default_value(self):
        position = Position()
        self.assertEqual(position.x, 0)
        self.assertEqual(position.y, 0)
    
    def test_position_with_values(self):
        position = Position(1, 2)
        self.assertEqual(position.x, 1)
        self.assertEqual(position.y, 2)
    
    
if __name__ == '__main__':
    unittest.main()
    