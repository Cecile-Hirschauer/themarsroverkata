import unittest
from mars_rover.plateau import Plateau
   
    
class Plateau(unittest.TestCase):
    
    def test_grid_size(self):
        plateau = Plateau(5, 5)
        self.assertEqual(plateau.width, 5)
        self.assertEqual(plateau.height, 5)
    
    
if __name__ == '__main__':
    unittest.main()