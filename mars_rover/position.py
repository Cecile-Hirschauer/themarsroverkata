class Position:
    
    def __init__(self, x=0, y=0):
        self.x = int(x)
        self.y = int(y)
        
    def __eq__(self, position):
        return self.x == position.x and self.y == position.y